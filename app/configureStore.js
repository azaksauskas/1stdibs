import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import app from './reducers';
import {createLogger} from 'redux-logger';
import {persistStore, createTransform, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const myTransform = createTransform(
  (state, key) => JSON.stringify(state, key),
  (state, key) => JSON.parse(state,key)
);
const reducer = persistReducer({
  transforms:[myTransform],
  key:'root',
  storage,
  blacklist:['item','cards','lookupCards']
}, app);



const configureStore = () => {
  let middlewares = [thunk];
  middlewares.push(createLogger());

  let store = createStore(
    reducer,
    applyMiddleware(...middlewares)
  );
  let persistor = persistStore(store);
  return {persistor, store};
}

export default configureStore;