import axios from 'axios';

export const receiveItem = (response) => ({
  type: 'RECEIVE_ITEM',
  response
});

export const fetchData = (id) => (dispatch) => {
  return axios.get(`http://localhost:3001/item/${id}/data`)
    .then(response => {
      dispatch(receiveItem(response.data))
    });
};