export const toggleFavorite = (id) => {
  return {
    type:'TOGGLE_FAVOURITE',
    id
  }
  
}