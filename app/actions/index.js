import axios from 'axios';

export const receiveCards = (response) => ({
  type: 'RECEIVE_CARDS',
  response
});

export const fetchData = (limit) => (dispatch, getState) => {
  const numberOfCards = getState().cards.length;
  return axios.get(`http://localhost:3001/data?start=${numberOfCards}&limit=${limit || 9 }`)
    .then(response => dispatch(receiveCards(response.data.items)));
};