import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './../actions/itemActions';
import { getCardItem } from './../reducers';
import styles from './style.less';
import Favourite from './Favourite';

const mapStateToProps = (state, { match: { params } }) => {
  const cardId = params.id;
  return {
    data: getCardItem(state, cardId),
    cardId
  }
}
class CardItem extends React.Component {
  componentDidMount() {
    const { cardId } = this.props;
    this.fetchData(cardId);
  }
  fetchData(cardId) {
    const { fetchData } = this.props;
    fetchData(cardId);
  }
  toggleFavorite(data) {
    const { addFav } = this.props;
    addFav(data);
  }
  render() {
    const { data, isFavorite, cardId } = this.props;
    const { title, seller, price, image, measurements, description, creators } = data;
    return (
      <div className={styles.item}>
        <div className={`${styles.header} ${styles.item__header}`}>
          <div className={styles.item__backButton}>
            <Link to="/">{'< Home'}</Link>
          </div>
          <span>{`${!!seller ? seller.company : ''}`}</span>
        </div>
        <div className={styles.item__wrapper}>
          <div className={styles.item__left}>
            <Favourite className={styles.item__favouriteWrapper} cardId={cardId} />
            <div className={styles.left__inner}>
              <img src={image} alt={title} />
            </div>
          </div>
          <div className={styles.item__right}>
            <div className={`${styles.right_block} ${styles.right__firstBlock}`}>
              <div className={styles.right__firstBlock_inner}>
                <div className={styles.item__title}>{title}</div>
                {!!price
                  ? <div>Price Upon Request</div>
                  : <div>{`${!!price ? price.amounts.USD : ''}`}</div>
                }
                <div className={styles.item__measurements}>
                  <div>Measurements:</div>
                  <span>{`${!!measurements ? measurements.display : ''}`}</span>
                </div>
              </div>
              <div className={styles.right__buttons}>
                <button>PURCHASE</button>
                <button>MAKE OFFER</button>
              </div>
            </div>
            <div className={`${styles.right_block} ${styles.right__secondBlock}`}>
              <div>
                {description}
              </div>
              <div className={styles.item__creator}>
                <span>Creator: {creators}</span>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}
CardItem = withRouter(connect(mapStateToProps, actions)(CardItem));

export default CardItem;