import React from 'react';
import {connect} from 'react-redux';
import * as actions from './../actions';
import {getCards, isFav} from './../reducers';
import styles from './style.less';
import CardsList from './CardsList';

const mapStateToProps = (state) => {
  return {
    cards: getCards(state)
  }
}
class CardsWrapper extends React.Component {
  loadMore() {
    this.getData();
  }
  componentDidMount() {
    this.getData();
  }
  getData(limit = 9) {
    const { fetchData } = this.props;
    fetchData(limit);
  }
  render() {
    const { cards } = this.props;
    return (
      <div className={styles.browse}>
        <div className={styles.header}>Browse page</div>
        <CardsList cards={cards} />
        <button className={styles.browse__loadMore} onClick={() => this.loadMore()}>Load More</button>
      </div>
    )
  }
}
CardsWrapper = connect(
  mapStateToProps,
  actions
)(CardsWrapper);

export default CardsWrapper;