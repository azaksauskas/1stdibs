import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './../configureStore';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CardItem from './CardItem';
import CardsWrapper from './CardsWrapper';

let {store} = configureStore();


ReactDOM.render(
  <Provider store={store}>
      <Router>
        <div>
          <Switch>
            <Route path="/" exact component={CardsWrapper} />
            <Route path="/item/:id" exact component={CardItem} />
          </Switch>
        </div>
      </Router>
  </Provider>,
  document.getElementById("root")
)
