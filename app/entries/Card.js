import React from 'react';
import {Link} from 'react-router-dom';
import styles from './style.less';
import Favourite from './Favourite';

const Card = ({ id, title, image, price, onClick}) => {
  return (
    <div className={styles.card__itemWrapper} >
      <div className={styles.card__inner}>
        <Link to={`/item/${id}`} >
          <img src={image} alt={title} />
        </Link>
        <div className={styles.card__bottom}>
          <span>{`${price ? price.amounts.USD : ''}`}</span>
            <Favourite cardId={id} />
        </div>
      </div>

    </div>
  )
}



export default Card;