import Card from './Card';
import React from 'react';
import styles from './style.less';

const CardsList = ({ cards, onFavClick, isFavorite }) => {
  return (
    <div className={styles.list__wrapper}>
      {
        cards.map(card => <Card key={card.id} {...card}>{card.title}</Card>)
      }
    </div>
  )
}
export default CardsList;