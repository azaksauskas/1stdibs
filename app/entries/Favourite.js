import favCard from './style.less';
import {connect} from 'react-redux';
import {isFavorite} from './../reducers/favourite';
import {toggleFavorite} from './../actions/favourite';
import React from 'react';

const mapStateToProps = (state) => {
  return {
    isFav: (id) => isFavorite(state, id)
  } 
}
const mapDispatchtoProps = (dispatch) => {
  return {
    toggleFavorite: (id) => dispatch(toggleFavorite(id))
  }
}

class Favourite extends React.Component{
  render(){
    const {isFav, cardId, toggleFavorite} = this.props;
    return (
      <div 
       className = {`${favCard.favourite__wrapper} ${!!isFav(cardId) ? favCard.favourite_active: ''}`} 
       onClick={() => toggleFavorite(cardId) }
       >
          Favourite
        </div>
    )
  }
}

Favourite = connect(mapStateToProps, mapDispatchtoProps)(Favourite);


export default Favourite;