export const isFavorite = (state, cardId) => {
  return ~state.fav.findIndex( id => id === cardId);
} 
export const fav = (state = [], action) => {
  switch(action.type){
    case 'TOGGLE_FAVOURITE':
      const {id} = action;
      const item = state.findIndex(favId => favId === id);
      return ~item
      ? [...state.filter(favId => favId !== id )]
      : [...state, id]
    default:
      return state;
  }
}