import {combineReducers} from 'redux';
import {fav} from './favourite';

const lookupCards = (state = {}, action) => {
  const {response} = action;
  switch(action.type){
    case 'RECEIVE_CARDS':
      const nextState = {...state};
      response.forEach( card => {
        nextState[card.id] = card;
      });
      return nextState;
    default:
      return state;
  }
}
const allCards = (state = [], action) => {
  switch (action.type) {
    case 'RECEIVE_CARDS':
      return [...state, ...action.response.map(card => card.id)];
    default:
      return state;
  }
}

const item = (state = {}, action) => {
  switch (action.type) {
    case 'RECEIVE_ITEM':
      return action.response;
    default:
      return state;
  }
}
const app = combineReducers({cards: allCards, lookupCards, item, fav});

export default app;

export const getCards = (state) => {
  return state.cards.map( id => state.lookupCards[id]);
}
export const getCardItem = (state) => {
  return state.item;
}

